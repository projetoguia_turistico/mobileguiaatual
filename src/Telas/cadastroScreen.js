import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text, Alert } from "react-native";
import api from "./axios/axios";
import { MaterialIcons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";

const CustomTextInput = ({ placeholder, value, onChangeText, secureTextEntry, icon, onPressIcon }) => {
  return (
    <View style={styles.inputContainer}>
      <TextInput style={styles.input} placeholder={placeholder} value={value} onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}/>

      <TouchableOpacity onPress={onPressIcon} style={styles.iconContainer}>
        <MaterialIcons name={icon} size={24} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const CadastroScreen = ({ navigation }) => {
  const [nome, setNome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [confirmarSenha, setConfirmarSenha] = useState("");

  const [mostrarSenha, setMostrarSenha] = useState(false);   
  const [mostrarConfirmarSenha, setMostrarConfirmarSenha] = useState(false);

  const handleCadastro = async () => {
    try {
      const response = await api.postUser({
        nome: nome,
        senha: senha,
        email: email,
        telefone: telefone,
        confirmarSenha: confirmarSenha,
      });
      // Adicione lógica para lidar com a resposta da API, como navegação ou exibição de alertas
    } catch (error) {
      // Adicione tratamento de erros
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>FAÇA O SEU CADASTRO AQUI!</Text>
      <TextInput style={styles.input} placeholder="Nome" value={nome} onChangeText={setNome} />
      <TextInput style={styles.input} placeholder="Email" value={email} onChangeText={setEmail} />
      <TextInput style={styles.input} placeholder="Telefone" value={telefone} onChangeText={setTelefone} />

      <CustomTextInput
        placeholder="Senha"
        secureTextEntry={!mostrarSenha}
        value={senha}
        onChangeText={setSenha}
        onPressIcon={() => setMostrarSenha(!mostrarSenha)}
        icon={mostrarSenha ? "visibility" : "visibility-off"}
      />

      <CustomTextInput
        placeholder="Confirmar Senha"
        secureTextEntry={!mostrarConfirmarSenha}
        value={confirmarSenha}
        onChangeText={setConfirmarSenha}
        onPressIcon={() => setMostrarConfirmarSenha(!mostrarConfirmarSenha)}
        icon={mostrarConfirmarSenha ? "visibility" : "visibility-off"}
      />

      <View style={styles.buttonContainer}>

        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("RGT")}>
          <Text style={styles.buttonText}>Início</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={handleCadastro}>
          <Text style={styles.buttonText}>Cadastrar</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Login")}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    paddingHorizontal: 16,
  },
  input: {
    height: 40,
    width: 260,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",  // Ajustado para alinhar o ícone com o texto
  },
  iconContainer: {
    position: "absolute",
    right: 10,  // Ajustado para posicionar o ícone à direita
    top: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: 35,
    marginTop: 20,
  },
  button: {
    backgroundColor: "orange",
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
  },
  headerText: {
    fontWeight: "bold",
    marginBottom: 15,
    fontSize: 18, // Adicione um tamanho de fonte adequado
  },
});

export default CadastroScreen;
