import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";

function InicialScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.TextInput}>BEM VINDO, VENHA CONHECER O DECIBELÍMETRO WEB!</Text>
      <View style={styles.buttonContainer}>
        <View style={styles.buttonWrapper}>
          <Button title="LOGIN" onPress={() => navigation.navigate("Login")} color="#060026" />
        </View>
        <View style={styles.buttonWrapper}>
          <Button title="CADASTRO" onPress={() => navigation.navigate("Cadastro")} color="#060026" />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center", 
    backgroundColor: "white", /* Cor de fundo */
  },
  buttonContainer: {
    flexDirection: "row", /* Alinha botões lado a lado */
    justifyContent: "space-between", 
    width: '20%', 
    marginTop: 40, /* Margem superior para separar do texto */
    
  },
  buttonWrapper: {
    flex: 1, 
    marginHorizontal: 10, /* Espaço horizontal entre os botões */
  },
  TextInput: {
    fontWeight: 'bold', /* Deixa o texto em negrito */
    fontSize: 20,
    marginBottom: 40, /* Espaço abaixo do texto */
    textAlign: "center", /* Alinha o texto no centro horizontalmente */
  }
});

export default InicialScreen;
