import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text, Alert } from "react-native";
import api from "./axios/axios";

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  const handleLogin = async () => {
    try {
      // Verifique se os campos de email e senha estão preenchidos
      if (!email || !senha) {
        Alert.alert("Erro", "Por favor, preencha todos os campos.");
        return;
      }
  
      const response = await api.postLogin({ email, senha,});

        console.log(response)
      if (response.status === 200) { 
        navigation.navigate("Home");
      } else {
        // Exiba um alerta de erro ao fazer login
        Alert.alert("Erro", response.data.message);
      }
    } catch (error) {
      // Se ocorrer um erro na requisição, exiba um alerta genérico
      Alert.alert(
        "Erro", "Ocorreu um erro ao fazer login. Verifique suas credenciais e tente novamente."
      );
    }
  };
  
  return (
    <View style={styles.container}>
      <Text style={styles.TextInput}>FAÇA LOGIN AQUI!!!</Text>
      <TextInput style={styles.input} placeholder="Email" onChangeText={setEmail} value={email}/>
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true} onChangeText={setSenha} value={senha}/>
      
      <View style={styles.buttonContainer}>
        <Button style={styles.button} title="Início" onPress={() => navigation.navigate("RGT")} color="orange"/>
        <Button style={styles.button} title="Entrar" onPress={handleLogin} color="orange"/>
        <Button style={styles.button} title="Cadastro" onPress={() => navigation.navigate("Cadastro")} color="orange"/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  input: {
    height: 40,
    width: 300,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: 35,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    fontSize: 16,
    color: "black",
  },
  TextInput: {
    fontWeight: "bold",
    marginBottom: 15,
  },
});

export default LoginScreen;
