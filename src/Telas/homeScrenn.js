import React from "react";
import { View, Text, Button, StyleSheet, ScrollView, Image, TouchableOpacity  } from "react-native";
import TabBar from "../../component/tabBar";

const HomeScreen = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <View style={styles.header}>
      <View style={styles.textContainer}>
          <Text style={styles.text}>ACESSE OS RELATÓRIOS AQUI!</Text>
        </View>
      </View>
      <TabBar />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    textAlign: "center",
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
    marginTop: 20,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    paddingHorizontal: 20,
    marginTop:9,
  },
  textContainer: {
    flex: 1,
    marginTop:90,
    marginLeft:"center",

  },
  buttonContainer: {
    position: "absolute",
    top: 10,
    right: 20,
  },
});

export default HomeScreen;
