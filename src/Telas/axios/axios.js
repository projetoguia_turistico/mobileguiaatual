import axios from "axios";

const api = axios.create({
    baseURL: "http://10.89.234.137:5000/api",
    headers: {
        'accept': 'application/json'
     },
});

const sheets = {
    getUser: () => api.get("/user"),
    postLogin: (user) => api.post("/login", user),
    postUser: (user) => api.post("/postUser", user),
    getGuia:() => api.get("/guia"),
    createReserva: (reserva) => api.post("/reserva", reserva)
};

export default sheets;