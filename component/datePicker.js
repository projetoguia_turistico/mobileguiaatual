import { useState } from "react";
import React from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DatePicker = ({ buttonTitle, dateKey, setReserva }) => {
  const [datePickerVisible, setdatePickerVisible] = useState(false);
  const showDatePicker = () => {
    setdatePickerVisible(true);
  };
  const hideDatePicker = () => {
    setdatePickerVisible(false);
  };
  const handleConfirm = (date) => {
    const formattedDate = date.toISOString().split("T")[0];
    console.log(formattedDate)
    setReserva((prevState) => ({
      ...prevState,
      [dateKey]: formattedDate,
    }));
    hideDatePicker();
    
  };

  return (
    <View>
      <Button title={buttonTitle} onPress={showDatePicker} color="orange" />
      {/* //o nome do botão vai vir com base no que tiver ocorrendo */}
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={"date"}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
}; // fim da DateTimePicker
export default DatePicker;
