import React from 'react';
import { Image, Text, StyleSheet, View } from 'react-native';

const Cabecalho = () => {
  return (
    <View style={styles.headerContainer}>
      {/* Descomente e ajuste se for necessário */}
      {/* <Image source={require('../assets/Logo.png')} style={styles.logo} /> */}
      <Text style={styles.textInput}>RGT</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#060026', // Cor de fundo do cabeçalho
    padding: 10, // Adiciona padding ao redor do conteúdo
    height: 80, // Ajuste a altura conforme necessário
    width: '4500%', // Ocupa toda a largura disponível
  },
  textInput: {
    color: 'white',
    marginLeft: 10,
    fontSize: 24, // Tamanho da fonte
    fontWeight: 'bold', // Texto em negrito
  },
  // logo: {
  //   width: 40,
  //   height: 60,
  //   marginLeft: 4,
  // },
});

export default Cabecalho;
