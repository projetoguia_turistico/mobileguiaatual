import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUser, faQuestionCircle, faHome } from '@fortawesome/free-solid-svg-icons';
import { useNavigation } from '@react-navigation/native';

//Tab do rodapé
const TabBar = () => {
  const navigation = useNavigation();

  const goToHome = () => {
    navigation.navigate('Home'); 
  };

  const PerfilUsuario = () => {
    navigation.navigate('Perfil'); 
  };

  return (
    <View style={styles.tabBar}>
      <TouchableOpacity onPress={goToHome} style={styles.tabItem}>
        <FontAwesomeIcon icon={faHome} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Ajuda')} style={styles.tabItem}>
        <FontAwesomeIcon icon={faQuestionCircle} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={PerfilUsuario} style={styles.tabItem}>
        <FontAwesomeIcon icon={faUser} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 60,
    width: '100%', 
    position: 'absolute',
    bottom: 0,
  },
  tabItem: {
    alignItems: 'center',
  },
  icon: {
    color: 'orange',
    fontSize: 55,
  },
});

export default TabBar;